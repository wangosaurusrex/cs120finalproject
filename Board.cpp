#include <assert.h>
#include <cctype>
#include <iostream>
#include <fcntl.h>

#include "Terminal.h"
#include "Game.h"
#include "Prompts.h"
#include "Chess.h"

///////////////
// Board //
///////////////

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i=0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i=0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}

// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.
Piece* Board::getPiece(Position position) const {
    if (validPosition(position))
        return m_pieces[index(position)];
    else {
        Prompts::outOfBounds();
        return nullptr;
    }
}

// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    } else {
        return it->second->newPiece(owner);
    }
}

void Board::str2pos(std::string input, Position& pos) {
    // converts valid string to Positiion struct. Call function for start and end
    // position and updated correspondent struct members of board.
    const char columns[8] = {'a','b','c','d','e','f','g','h'};
    //declare variables to store row and columns embeded in string
    unsigned int r = input[1] - '1';
    unsigned int c = 0;
    while(input[0] != columns[c]){c++;}
    pos.x = c;
    pos.y = r;
}

int Board::makeMove(Position start, Position end) {
    // This method may handle the parts of moving pieces that
    // generalize over different board games
    int inCheck = this->check();
    //Do nothing if user attempts to move empty esquare    
    Piece* currPiece = this->getPiece(start);
    if(currPiece == nullptr){
        Prompts::noPiece();
        return 0;
    }
    //define variables to flag whether a given move is valid or not
    //ARRANGE IN THE CORRECT ORDER FOR WARNINGS
    int _validMove = currPiece->validMove(start, end, *this);
    if (_validMove == 0) {
        Prompts::noPiece();
        return 0;
    } else if (_validMove == -1) {
        Prompts::illegalMove();
        return 0;
    } else if (_validMove == 2 && (this->getPiece(end)->owner() == this->playerTurn())) {
        Prompts::blocked();
        return 0;
    }

    int _capture = 1; //preset it to one in order to allow regular moves
    Piece* capturedPiece;
    int id;
    Player owner;
    if (_validMove == 2 && (capturedPiece = currPiece->capture(end, *this)) == nullptr) {
        _capture = 0;
    }
    if (_validMove && _capture) {
        //Find 1D index from 2D initial and final positions. Use 
        unsigned int start1D = this->index(start);
        unsigned int end1D = this->index(end);
        //use iter_swap function to swap to vector elements
        if (_validMove == 2) {
            id = capturedPiece->id();
            owner = capturedPiece->owner();
            delete capturedPiece;
            this->setVector(end, nullptr);
        }
        std::iter_swap(m_pieces.begin()+start1D, m_pieces.begin()+end1D);
        this->pawn2queen();
        
        //check for checking situations
        if ((inCheck && this->check()-1 == this->playerTurn()) || (inCheck && this->check() == 3)) {
           Prompts::mustHandleCheck();
           std::iter_swap(m_pieces.begin()+end1D, m_pieces.begin()+start1D);
           if (_validMove == 2 ) { this->initPiece(id, owner, end); }
           return 0;
        } else if (this->check() - 1 == this->playerTurn() || this->check() == 3) {
           Prompts::cantExposeCheck();
           std::iter_swap(m_pieces.begin()+end1D, m_pieces.begin()+start1D);
           if (_validMove == 2) { this->initPiece(id, owner, end); }
           return 0;
        } 
        
        if (inCheck == 0 || (inCheck != 3 && inCheck - 1 != this->playerTurn())) {
            this->setTurn(1);
            if ((this->check() != 0 && this->check() - 1 != (this->playerTurn() - 1)) || this->check() == 3) {
                this->setTurn(-1);
                Prompts::check(this->playerTurn());
            } else {
                this->setTurn(-1);
            }
        }

        
        if (_validMove == 2) {
            return 2;
        }
        return 1;
    } else {
        return 0;
    }
    return 0;
}

void Board::pawn2queen(){
    //check if any white pawn made it to the top row
    Piece* edgeRowPiece;
    for(unsigned int c = 0; c < 8; c++){
        Position p(c,7);
        edgeRowPiece = getPiece(p);
        if((edgeRowPiece != nullptr) &&(edgeRowPiece->id() == PAWN_ENUM) && (edgeRowPiece->owner() == WHITE)){
            delete edgeRowPiece;
            this->setVector(p, nullptr);
            initPiece(QUEEN_ENUM,WHITE,p);
        }
    }
    edgeRowPiece = nullptr;
    for(unsigned int c = 0; c < 8; c++){
        Position p(c,0);
        edgeRowPiece = getPiece(p);
        if((edgeRowPiece != nullptr)&&(edgeRowPiece->id() == PAWN_ENUM) && (edgeRowPiece->owner() == BLACK)){
            delete edgeRowPiece;
            this->setVector(p, nullptr);
            initPiece(QUEEN_ENUM,BLACK,p);
        }
    }
}


void Board::setVector(Position position, Piece* piece) {
    int index = this->index(position);
    m_pieces[index] = piece;
}


int Board::checkMate() {
    int valid = 0;
    int count = 0;
    for (unsigned int j = 8; j > 0; j--) {
        for (unsigned int i = 0; i < 8; i++) {
            Position p(i, j - 1);
            if (this->getPiece(p) != nullptr && this->getPiece(p)->owner() != this->playerTurn()) {
                for (unsigned int r = 8; r > 0; r--) {
                    for (unsigned int s = 0; s < 8; s++) {
                        count++;
                        Position q(s, r - 1);
                        unsigned int start1D = this->index(p);
                        unsigned int end1D = this->index(q);
                        if (this->getPiece(q) != nullptr) {
                            Piece* end = this->getPiece(q);
                            int endID = end->id();
                            Player endOwner = end->owner();
                            this->setTurn(1);
                            int result = this->checkMateMove(p, q);
                            this->setTurn(-1);
                            if (result == 1) {
                                std::iter_swap(m_pieces.begin()+start1D, m_pieces.begin()+end1D);
                                valid++;
                            } else if (result == 2) {
                                initPiece(endID, endOwner, p);
                                std::iter_swap(m_pieces.begin()+start1D, m_pieces.begin()+end1D);
                                valid++;
                            }
                        } else {
                            this->setTurn(1);
                            int result = this->checkMateMove(p, q);
                            this->setTurn(-1);
                            if (result) {
                                std::iter_swap(m_pieces.begin()+start1D, m_pieces.begin()+end1D);
                                valid++;
                            }
                        }
                    }
                }
            }
        }
    }
    this->setTurn(1);
    if (valid == 0 && this->check() == 0) {
        this->setTurn(-1);
        return 2;
    } else if (valid != 0) {
        this->setTurn(-1);
        return 0;
    }
    this->setTurn(-1);
    return 1;
}

int Board::checkMateMove(Position start, Position end) {
    //Do nothing if user attempts to move empty esquare   
    int inCheck = this->check();
    Piece* currPiece = this->getPiece(start);
    if(currPiece == nullptr){
        return 0;
    }
    //define variables to flag whether a given move is valid or not
    //ARRANGE IN THE CORRECT ORDER FOR WARNINGS
    int _validMove = currPiece->validMove(start, end, *this);
    if (_validMove == 0) {
        return 0;
    } else if (_validMove == -1) {
        return 0;
    } else if (_validMove == 2 && (this->getPiece(end)->owner() == this->playerTurn())) {
        return 0;
    }
    int _capture = 1; //preset it to one in order to allow regular moves
    
    Piece* capturedPiece;
    int id;
    Player owner;
    if (_validMove == 2 && (capturedPiece = currPiece->capture(end, *this)) == nullptr) {
        _capture = 0;
    }
    if (_validMove && _capture) {
        //Find 1D index from 2D initial and final positions. Use 
        unsigned int start1D = this->index(start);
        unsigned int end1D = this->index(end);
        //use iter_swap function to swap to vector elements
        if (_validMove == 2) {
            id = capturedPiece->id();
            owner = capturedPiece->owner();
            delete capturedPiece;
            this->setVector(end, nullptr);
        }
        std::iter_swap(m_pieces.begin()+start1D, m_pieces.begin()+end1D);

        if ((inCheck && this->check()-1 == this->playerTurn()) || (inCheck && this->check() == 3)) {
           std::iter_swap(m_pieces.begin()+end1D, m_pieces.begin()+start1D);
           if (_validMove == 2 ) { this->initPiece(id, owner, end); }
           return 0;
        } else if (this->check() - 1 == this->playerTurn() || this->check() == 3) {
           std::iter_swap(m_pieces.begin()+end1D, m_pieces.begin()+start1D);
           if (_validMove == 2) { this->initPiece(id, owner, end); }
           return 0;
        } 
        
        if (_validMove == 2) {
            return 2;
        }
        return 1;
    } else {
        return 0;
    }
    return 0;

}

int Board::check() {
    Position blackKing;
    Position whiteKing;
    int whiteCheck = 0;
    int blackCheck = 0;
    for (unsigned int j = 8; j > 0; j--) {
        for (unsigned int i = 0; i < 8; i++) {
            Position p(i, j - 1);
            if (this->getPiece(p) != nullptr && this->getPiece(p)->id() == KING_ENUM) {
                if (this->getPiece(p)->owner() == BLACK) {
                    blackKing = p;
                } else {
                    whiteKing = p;
                }
            }
        }
    }

    for (unsigned int j = 8; j > 0; j--) {
       for (unsigned int i = 0; i < 8; i++) {
           Position p(i, j - 1);
           if (this->getPiece(p) != nullptr && this->getPiece(p)->owner() != this->playerTurn()) {
               this->setTurn(1);
               if (this->playerTurn() == WHITE && this->getPiece(p)->validMove(p, blackKing, *this) == 2) {
                    blackCheck++;
               }
               if (this->playerTurn() == BLACK && this->getPiece(p)->validMove(p, whiteKing, *this) == 2) {
                    whiteCheck++;
               }
               this->setTurn(-1);
           }
       }
    }
    if (blackCheck == 0 && whiteCheck == 0) {
        return 0;
    } else if (whiteCheck > 0 && blackCheck == 0) {
        return 1;
    } else if (blackCheck > 0 && whiteCheck == 0) {
        return 2;
    } else {
        return 3;
    }
    return 0;
}

void coloredSpace(int seed){
    Terminal background;
    background.colorBg(background.WHITE);
    std::cout << "     ";
    for (unsigned int i = 0; i < 8; i++) {
        if (seed%2 == 0){
            background.colorBg(background.RED);
        } else {
            background.colorBg(background.BLUE);
        }
        std::cout << "       ";
        seed++;
    }
    background.set_default();
    std::cout << std::endl;

}

void Board::displayBoard() {
    int printCount = 0;
    Terminal background;
    Terminal foreground;
    for (unsigned int j = 8; j > 0; j--) {
        coloredSpace(printCount);
        background.colorBg(background.WHITE);
        foreground.colorFg(false,foreground.BLACK);
        std::cout << "  " << j << "  ";
        for (unsigned int i = 0; i < 8; i++) {
            //setup colors
            if(printCount%2 == 0) {
                background.colorBg(background.RED);
            } else {
               background.colorBg(background.BLUE);
            }
            Position p = Position(i, j - 1);
            if (this->getPiece(p) == nullptr) {
                std::cout << "       ";
                printCount++;
            } else {
                if (this->getPiece(p)->owner() == 0) {
                    std::cout << "   ";
                    foreground.colorFg(true,foreground.WHITE);
                    switch (this->getPiece(p)->id()) {
                        case 1:
                            std::cout << u8"\u2659";
                            break;
                        case 2:
                            std::cout << u8"\u2656";
                            break;
                        case 3:
                            std::cout << u8"\u2658";
                            break;
                        case 4:
                            std::cout << u8"\u2657";
                            break;
                        case 5:
                            std::cout << u8"\u2655";
                            break;
                        case 6:
                            std::cout << u8"\u2654";
                            break;
                        default:
                            std::cout << " " << std::endl;
                            break;
                    }
                    std::cout << "   ";
                } else {
                    std::cout << "   ";
                    foreground.colorFg(false,foreground.BLACK);
                    switch(this->getPiece(p)->id()) {
                        case 1:
                            std::cout << u8"\u265F";
                            break;
                        case 2:
                            std::cout << u8"\u265C";
                            break;
                        case 3:
                            std::cout << u8"\u265E";
                            break;
                        case 4:
                            std::cout << u8"\u265D";
                            break;
                        case 5:
                            std::cout << u8"\u265B";
                            break;
                        case 6:
                            std::cout << u8"\u265A";
                            break;
                        default:
                            std::cout << " " << std::endl;
                            break;
                    }
                    std::cout << "   ";
                }
            printCount++;
            }
        }
        background.set_default();
        std::cout << std::endl;
        coloredSpace(printCount);
        printCount++;
        if(printCount%2 == 0) {
            background.colorBg(background.BLUE);
        } else {
               background.colorBg(background.RED);
        }
    }
    background.colorBg(background.WHITE);
    std::cout << "                                                             ";
    background.set_default();
    std::cout << std::endl;
    background.colorBg(background.WHITE);
    foreground.colorFg(false,foreground.BLACK);
    std::cout << "        a      b      c      d      e      f      g      h   ";
    background.set_default();
    std::cout << std::endl;
    background.colorBg(background.WHITE);
    std::cout << "                                                             ";
    background.set_default();
    std::cout << std::endl;
}

