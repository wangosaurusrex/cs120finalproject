#include "Game.h"

int Piece::validMove(Position start, Position end, const Board& board) const {
    std::cout << "Piece class validMove" << std::endl;
    Player currPlayer = board.playerTurn();
    /*if (this->owner() != currPlayer) {
        Prompts::noPiece();
        return 0;
    }

    if (this->owner() == currPlayer) {
        Prompts::blocked();
        return 0;
    }*/

    return 0;
}

void Board::str2pos(std::string input, Position& pos) {
    const char columns[8] = {'a','b','c','d','e','f','g','h'};
    unsigned int r = input[1] = '1';
    unsigned int c = 0;
    while (input[0] != columns[c]) {
        c++;
    }
    pos.x = c;
    pos.y = r;
}

int Board::makeMove(Position start, Position end) {
    Piece* currPiece = this->getPiece(start);
    std::cout << "check validMove" << std::endl;
    //std::cout << currPiece->id() << std::endl;
    if (currPiece->validMove(start, end, *this)) {
        unsigned int start1D = this->index(start);
        unsigned int end1D = this->index(end);

        std::iter_swap(m_pieces.begin() + start1D, m_pieces.begin() + end1D);
        return 1;
    } else {
        return 0;
    }
    return 0;
}
