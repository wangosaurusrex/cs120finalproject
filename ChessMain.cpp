
#include <stdio.h>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"

int main() {
    ChessGame chess;
    if (chess.mainMenu()) {
        if (chess.readFile() < 0) {
            return 1;
        }
    } else {
        chess.setupBoard();
    }
    chess.play();
    return 0;
}
