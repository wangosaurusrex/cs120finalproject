#ifndef GAME_H
#define GAME_H
#include <vector>
#include <map>
#include <cmath>
//#include "Prompts.h"

// The list of players
enum Player {
    WHITE = 0,
    BLACK = 1
};
#include "Prompts.h"

// We need to pre-declare these to allow for cross-dependencies
class Piece;
class AbstractPieceFactory;
class Board;

// A struct to store the position
// [DO NOT MODIFY]
struct Position {
    unsigned int x, y;
    Position(unsigned int xx = 0 , unsigned int yy = 0) : x(xx) , y(yy) { }
};

// An individual piece
class Piece {
public:
    // No public constructor: only use factories to build this
    // class's subclasses
    
    virtual ~Piece() {}

    // Returns the owner of the piece
    // [DO NOT MODIFY]
    Player owner() const { return m_owner; }

    // Returns the id of the piece
    // [DO NOT MODIFY]
    int id() const { return m_id; }

    // Returns an integer representing move validity
    // >= 0 = valid, < 0 = invalid
    // [Do not modify the type of this method]
    virtual int validMove(Position start, Position end, const Board& board) const; 

    int pathFree(Position start, Position end, const Board& board) const;

    Piece* capture(Position end, Board& board) const;

protected:
    Player m_owner;
    int m_id;

    // Constructs a piece with a specified owner
    // Note that this is deliberately made protected. Use the factory only!
    // [DO NOT MODIFY]
    Piece(Player owner, int id) : m_owner(owner) , m_id(id) {}
};


// The board for the game
class Board {
    // The type of a piece factory map. Maps from int describing a
    // Piece to the factory class producing the Piece.
    typedef std::map<int, AbstractPieceFactory*> PieceGenMap;
public:
    // Construct a board with the specified dimensions
    Board(unsigned int w, unsigned int h, int turn = 1) :
        m_width(w), m_height(h), m_turn(turn), m_pieces(w * h, nullptr) {}

    // Virtual destructor is necessary for a class with virtual methods
    virtual ~Board();

    // Returns the width of the board
    // [DO NOT MODIFY]
    unsigned int width() const { return m_width; }

    // Returns the height of the board
    // [DO NOT MODIFY]
    unsigned int height() const { return m_height; }

    // Create a piece on the board using the factory.
    // Returns true if the piece was successfully placed on the board
    // [DO NOT MODIFY]
    bool initPiece(int id, Player owner, Position p);
 
    // Returns a pointer to the piece at the specified position,
    // if the position is valid and occupied, nullptr otherwise.
    // [DO NOT MODIFY]
    Piece* getPiece(Position p) const;

    // Returns the player whose turn it is
    // [DO NOT MODIFY]
    Player playerTurn() const { return static_cast<Player>(!(m_turn % 2)); }

    // Return the current turn
    // [DO NOT MODIFY]
    int turn() const { return m_turn; }

    // Returns true if the position is within bounds
    // [DO NOT MODIFY]
    bool validPosition(Position p) const {
        return p.x < m_width && p.y < m_height;
    }
    
    //converts a string ot a position
    void str2pos(std::string input, Position& pos);

    //replaces pawn with queen when it reaches the end;
    void pawn2queen();
    
    //displays the board
    void displayBoard();

    //changes values in the vector where pieces are stored
    void setVector(Position position, Piece* piece);

    //checks for a check, returns 0 if no one in check, 1 if white in check,
    //2 if black in check, 3 if both in check.
    int check();

    //checks for checkmate or stalemate, returns 0 if none, 1 if checkmate,
    //2 if stalemate
    int checkMate();

    //special makeMove function for checkmate
    int checkMateMove(Position start, Position end);

    // Perform a move from the start Position to the end Position
    // The method returns an integer with the status
    // >= 0 is SUCCESS, < 0 is failure:w
    //
    // [Do not modify the type of this method]
    virtual int makeMove(Position start, Position end);

    //manually change the turn;
    virtual void setTurn(int i) {
        m_turn += i;
    }

    // The main gameplay loop. Ideally, you should be able to implement
    // all of the gameplay loop logic here in the Board class rather than
    // overriding this method in the specialized Game-specific class
    virtual void run() {
        //ChessGame::setUpBoard();
        
        //Board::displayBoard();
    }

    // Returns "true" if the game is over
    virtual bool gameOver() const = 0 ;

protected:
    // All the factories registered with this Board
    PieceGenMap m_registeredFactories;

    unsigned int m_width , m_height;

    // Current game turn
    int m_turn;

    //Structs to store initial ad final positions entered by players
    Position startPos;
    Position endPos;

    // Vector containing all the Pieces of the board
    std::vector<Piece*> m_pieces;

    // Get the 1d index from the 2d position
    // [DO NOT MODIFY]
    unsigned int index(Position position) const {
        return position.y * m_width + position.x;
    }
    
    // Functionality for creating a new piece (called by initPiece)
    // [DO NOT MODIFY]
    Piece* newPiece(int id, Player owner);
   
    // Functionality for adding piece factories (called by constructor)
    // [DO NOT MODIFY]
    bool addFactory(AbstractPieceFactory*);
};





// A (virtual) class responsible for creating new instances of a
// particular type of piece (factory pattern)
// [DO NOT MODIFY]
class AbstractPieceFactory {
public:
    // Create a piece with the specified owner
    virtual Piece* newPiece(Player owner) const = 0;
    virtual ~AbstractPieceFactory() {}
};

// A templated class generating Pieces
// [DO NOT MODIFY]
template <class T>
class PieceFactory : public AbstractPieceFactory {
public:
    PieceFactory(int id) : m_id(id) {}
    Piece* newPiece(Player owner) const override {
        return new T(owner, m_id);
    }
protected:
    int m_id;
};


#endif // GAME_H
