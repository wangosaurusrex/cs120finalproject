#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Chess.h"
#include "Prompts.h"
#include <boost/algorithm/string.hpp>
#include <string>
#include <ctype.h>
#include <fstream>
#include <cctype>

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    // Possibly implement chess-specific move logic here
    //
    // We call Board's makeMove to handle any general move logic
    // Feel free to use this or change it as you see fit
    int retCode = Board::makeMove(start, end);
    return retCode;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}

//main menu
int ChessGame::mainMenu() {
    while (true) {
        Prompts::menu();
        std::string input;
        std::cin >> input;
        if (input == "2") {
            return 1;
        } else if (input == "1") {
            return 0;
        }
    }
}

//main game operation logic
void ChessGame::play() {
    //Game control variable
    bool gameNotOver = true;
    int showBoard = 0;
    //declare string to collect user input
    std::string input;
    this->run();
    while (gameNotOver) {
        if (showBoard) {
        //if board is toggled to be displayed
        this->displayBoard();
        }
        Prompts::playerPrompt(this->playerTurn(), this->turn());
        std::cin >> input;
        //use boost::iequals for case insensitive comparison of strings
        if (boost::iequals(input, "q")) {
            gameNotOver = false;

        } else if (boost::iequals(input, "board")) { //toggle display board
            showBoard = (showBoard + 1) % 2;
            this->setTurn(-1);
            std::cin.clear(); //clear any remaining input
            std::cin.ignore(INT_MAX, '\n');

        } else if (boost::iequals(input, "save")) { //save game
            this->writeFile(this->turn());
            this->setTurn(-1);
            std::cin.clear(); //clear any remaining input
            std::cin.ignore(INT_MAX, '\n');

        } else if (boost::iequals(input, "forfeit")) { //forfeit game
            this->setTurn(1);
            Prompts::win(this->playerTurn(), this->turn());
            Prompts::gameOver();
            gameNotOver = false;
            std::cin.clear(); //clear any remaining input
            std::cin.ignore(INT_MAX, '\n');
        } else if (validCoor(input)) { 
            //make sure first coordinate is valid (is on the board)
            input[0] = tolower(input[0]);
            std::string to;
            std::cin >> to;
            if (validCoor(to)) { //make sure second coordinate is valid (is on the board)
                //make move
                to[0] = tolower(to[0]);
                this->str2pos(input,startPos); //converts coordinates to struct Position
                this->str2pos(to,endPos); //which is member of Board class
                if (!this->makeMove(startPos, endPos)) {
                    this->setTurn(-1);
                }
                int cm = this->checkMate(); //check for checkmate or stalemate
                if (cm == 1) {
                    Prompts::win(this->playerTurn(), this->turn());
                    Prompts::gameOver();
                    gameNotOver = false;
                } else if (cm == 2) {
                    Prompts::staleMate();
                    Prompts::gameOver();
                    gameNotOver = false;
                }
            } else { //if second coordinate is out of bounds
                this->setTurn(-1);
                std::cin.clear(); //clear any remaining input
                std::cin.ignore(INT_MAX, '\n');
            }
        } else { //if first coordinate is out of bounds
            this->setTurn(-1);
            std::cin.clear(); //clear any remaining input
            std::cin.ignore(INT_MAX, '\n');
        }
    this->setTurn(1);
    }
}

//checks if string is a valid coordinate
bool ChessGame::validCoor(std::string s) {
    if (s.length() != 2) {
        Prompts::parseError();
        return false;
    } else if (isalpha(s.at(0)) && isdigit(s.at(1))) {
        s[0] = tolower(s[0]);
        if (s[0] >= 'a' && s[0] <= 'h' && s[1] >= '1' && s[1] <= '8') {
            return true;
        } else {
            Prompts::outOfBounds();
            return false;
        }
    }
    Prompts::parseError();
    return false;
}

//reads in a file
int ChessGame::readFile() {
    Prompts::loadGame();
    std::string fileName; 
    std::cin >> fileName;
    std::ifstream file;
    file.open(fileName);
    std::string game;
    int turn;
    if (file.is_open()) {
        file >> game;
        if (game != "chess") {
            Prompts::loadFailure();
            file.close();
            return -1;
        }
        file >> turn;
        this->setupFromFile(file); 
        this->setTurn(turn - 1);
    } else {
        Prompts::loadFailure();
        return -1;
    }
    return 0;
}

//sets up a game from a file
int ChessGame::setupFromFile(std::ifstream& file) {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    std::string str;
    int count = 0;
    while (file >> str) {
        int player, piece, row, column;
        if (count % 3 == 0) {
            player = str[0] - '0';
        } else if (count % 3 == 1) {
            column = str[0] - 'a';
            row = str[1] - '0' - 1;
        } else if (count % 3 == 2) {
            piece = str[0] - '0';
            initPiece(piece, (Player) player, Position(column, row));
        }
        count++;
    }
    return 0;
}

//writes a game to a file
int ChessGame::writeFile (int turn) {
    Prompts::saveGame();
    std::string fileName;
    std::cin >> fileName;
    std::ofstream file;
    file.open(fileName);
    file << "chess\n";
    file << turn << '\n';
    for (unsigned int j = 8; j > 0; j--) {
        for (unsigned int i = 0; i < 8; i++) {
            Position p = Position(i, j - 1);
            if (this->getPiece(p) != nullptr) {
                file << this->getPiece(p)->owner() << " " << (char) (i + 'a') << j
                    << " " << this->getPiece(p)->id() << '\n';
            }
        }
    }
    file.close();
    return 0;
}

//checks if pawn is making its first move
int Pawn::firstMove(Position start, const Board& board) const {
    // Checks if pawn is making its first move. Returns 1 if it is, 0 otherwise
    Player currPlayer = board.playerTurn();
    if (currPlayer == WHITE && (start.y != 1)){
        //check if pawn is on 2nd row
        return  0;
    } else if (currPlayer == BLACK && (start.y !=6)){
        //check if pawn is on 7th row
        return 0;
    }
    return SUCCESS;

}

//get player who's making the move
int Pawn::validMove(Position start, Position end, const Board& board) const {
    int valid = Piece::validMove(start, end, board);
    if (!valid) {
        return 0;
    } else {
        Player currPlayer = board.playerTurn();
        int deltaCol = (int)(end.x - start.x);
        int deltaRow =(int)( end.y - start.y);
        switch (currPlayer) {
            case WHITE:
                if (this->firstMove(start,board)){
                   //cover possibilities of first move
                   if((deltaCol == 0) && ((deltaRow == 1) || (deltaRow == 2))){
                       if (board.getPiece(end) != nullptr) {
                           return -1;
                       }
                       return valid;
                   } else if (deltaRow == 1 && abs(deltaCol) == 1) {
                       Piece* endPiece = board.getPiece(end);
                       if (endPiece == nullptr) {
                            return -1;
                       } else {
                            return valid;
                       }
                   } else {
                       return -1;
                   }
                } else if ((deltaCol == 0) && (deltaRow == 1)){
                   //cover regular move
                   if (board.getPiece(end) != nullptr) {
                        return -1;
                   }
                   return valid;
                } else if (deltaRow == 1 && abs(deltaCol) == 1) {
                    Piece* endPiece = board.getPiece(end);  
                    if (endPiece == nullptr){
                        return -1; 
                    } else {
                        return valid;
                    }
                } else {
                   return -1;
                }
                break;
            case BLACK:
                    if (this->firstMove(start,board)){
                    //cover possibilities of first move
                    if((deltaCol == 0) && ((deltaRow == -1) || (deltaRow == -2))){
                        if (board.getPiece(end) != nullptr) {
                            return -1;
                        }
                        return valid;
                    } else if (deltaRow == -1 && abs(deltaCol) == 1) {
                       Piece* endPiece = board.getPiece(end);
                       if (endPiece == nullptr) {
                            return -1;
                       } else {
                            return valid;
                       }
                    } else {
                        return -1;
                    }
                } else if ((deltaCol == 0) && (deltaRow == -1)){
                    //cover regular move
                    if (board.getPiece(end) != nullptr) {
                        return -1;
                    }
                    return valid;
                } else if (deltaRow == -1 && abs(deltaCol) == 1) {
                    Piece* endPiece = board.getPiece(end);  
                    if (endPiece == nullptr){
                        return -1;
                    } else {
                        return valid;
                    }
                } else {
                    return -1;
                }
                break;
        }        
    }
    return 0;
}

//rook validmove
int Rook::validMove(Position start, Position end, const Board& board) const { 
    
    int valid = Piece::validMove(start, end, board);    
    if (!valid) {
        return 0;
    }

    int deltaCol = (int)(end.x - start.x);
    int deltaRow = (int)(end.y - start.y);

    if ((deltaCol == 0 && deltaRow != 0) || (deltaCol != 0 && deltaRow == 0)) {
        return valid;
    } else {
        return -1;
    }

    return SUCCESS;
}

//knight validmove
int Knight::validMove(Position start, Position end, const Board& board) const { 
        
    int valid = Piece::validMove(start, end, board);
    if (!valid) {
        return 0;
    }

    int deltaCol = abs((int)(end.x - start.x));
    int deltaRow = abs((int)(end.y - start.y));

    if ((deltaCol == 1 && deltaRow == 2) || (deltaCol == 2 && deltaRow == 1)) {
        return valid;
    } else {
        return -1;
    }
}

//bishop validmove
int Bishop::validMove(Position start, Position end, const Board& board) const {
        
    int valid = Piece::validMove(start, end, board);
    if (!valid) {
        return 0;
    }


    int deltaCol = abs((int)(end.x - start.x));
    int deltaRow = abs((int)(end.y - start.y));

    if (deltaCol == deltaRow && deltaCol != 0) {
        return valid;
    } else {
        return -1;
    }
}

//queen validmove
int Queen::validMove(Position start, Position end, const Board& board) const {
    
    int valid = Piece::validMove(start, end, board);
    if (!valid) {
        return 0;
    }

    int deltaCol = abs((int)(end.x - start.x));
    int deltaRow = abs((int)(end.y - start.y));

    if ((deltaCol == deltaRow && deltaCol != 0) || (deltaCol != 0 && deltaRow == 0) || (deltaCol == 0 && deltaRow != 0)) {
        return valid;
    } else {
        return -1;
    }
}

//king validmove
int King::validMove(Position start, Position end, const Board& board) const { 
    
    int valid = Piece::validMove(start, end, board);
    if (!valid) {
        return 0;
    }

    int deltaCol = abs((int)(end.x - start.x));
    int deltaRow = abs((int)(end.y - start.y));

    if ((deltaCol == 1 && deltaRow == 0) || (deltaRow == 1 && deltaCol == 0) || (deltaRow == 1 && deltaCol == 1)) {
        return valid;
    } else {
        return -1;
    }
}

