#include "Game.h"
#include "Chess.h"

int Piece::validMove(Position start, Position end, const Board& board) const {
    Player currPlayer = board.playerTurn();
    if (this->owner() != currPlayer) {
        return 0;
    }
    return this->pathFree(start, end, board);
}

int Piece::pathFree(Position start, Position end, const Board& board) const {
// chekc if there aren't pieces on the way of the piece that is currently being moved.
// Returns 0 if path is blocked, 1 if path is free and there's no piece at the end
// position, and 2 if path is free but there's a piece at the end position.
    int deltaRow = (int)(end.y - start.y);
    int deltaCol = (int)(end.x - start.x);
    int endOccup = 0; //variable used to check if end position contains a piece or not
    int iteration = 0;
    //define pointer that will be used to iterate over squares.
    Position square(end.x,end.y);
    Piece *squarePiece;

    
    if ((board.getPiece(start)->id() == KNIGHT_ENUM)){
        squarePiece = board.getPiece(square);
        if (squarePiece != nullptr){
        //for the knight, we only have to check if the last position is occupied
            return 2;
        }

    } else if ((abs(deltaRow) == abs(deltaCol)) && (deltaRow != 0)){
        //check for diagonal movements
        while((square.x != start.x) && (square.y != start.y)){
           squarePiece = board.getPiece(square);
           if (squarePiece != nullptr){
               if (iteration){
                   return 0;
               } else {
                   //if in the first iteration the pointer is null, it means that
                   //the end position contains no piece
                   endOccup = 1;
               }
           }
           //udpate of square coordinates depens on signs of deltaRow and 
           //deltaCol:
           if ((deltaRow > 0) && (deltaCol > 0)){
               square.x--;
               square.y--;
           } else if ((deltaRow > 0) && (deltaCol < 0)){
               square.x++;
               square.y--;
           } else if ((deltaRow < 0) && (deltaCol > 0)){
               square.x--;
               square.y++;
           } else if ((deltaRow < 0) && (deltaCol < 0)){
               square.x++;
               square.y++;
           }
           iteration++;
        }

    } else if((deltaRow == 0) && (deltaCol != 0)) {
        //check for horizontal movements
        while(square.x != start.x){
           squarePiece = board.getPiece(square);
           if (squarePiece != nullptr){
               if (iteration){
                   return 0;
               } else {
                   endOccup = 1;
               }
           }
           //udpate of square coordinates depends on sign of deltaCol:
           if(deltaCol > 0){
               square.x--;
           } else {
               square.x++;
           }
           iteration++;
        }

    } else if ((deltaCol == 0) && (deltaRow != 0)) {
        //check fof vertical movements
        while(square.y != start.y){
           squarePiece = board.getPiece(square);
           if (squarePiece != nullptr){
               if (iteration){
                   return 0;
               } else {
                   endOccup = 1;
               }
           }
           //udpate of square coordinates depends on sign of deltaCol:
           if(deltaRow > 0){
               square.y--;
           } else {
               square.y++;
           }
           iteration++;
        }
    }
    //check if final position is not occupied for knight movement
    if (endOccup){
       return 2;
    }

    return 1;
}
        
Piece* Piece::capture(Position end, Board& board) const {

//Performs capture of oponents piece if appropriate
    Player currPlayer = board.playerTurn();
    Piece *endPiece = board.getPiece(end);
    if (endPiece->id() == KING_ENUM) {
        return nullptr;
    }
    if(endPiece->owner() == currPlayer){
        //if player attempts to capture his own piece
        return nullptr;
    }
    return endPiece;
}
