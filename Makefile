CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

chess: ChessMain.o Chess.o Board.o Piece.o
	$(CXX) $(CXXFLAGS) ChessMain.o Chess.o Board.o Piece.o -o chess

unittest: unittest.o Chess.o Board.o Piece.o
	$(CXX) $(CXXFLAGS) unittest.o Chess.o Board.o Piece.o -o unittest

Board.o: Game.h
Chess.o: Game.h Chess.h Prompts.h
Piece.o: Game.h

clean:
	rm *.o chess

