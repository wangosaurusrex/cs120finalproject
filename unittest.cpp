#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include <string>

void pawnTest() {
    std::cout << "TESTING PAWN" << std::endl;
    ChessGame chess;
    chess.initPiece(PAWN_ENUM, WHITE, Position(0, 1));
    std::cout << "Expect no error messages" << std::endl;
    chess.makeMove(Position(0, 1), Position(0, 3));
    chess.makeMove(Position(0, 3), Position(0, 4));
    std::cout << "Expect an invalid move" << std::endl;
    chess.makeMove(Position(0, 3), Position (0, 1));
    std::cout << std::endl << std::endl;
}

void rookTest() {
    std::cout << "TESTING ROOK" << std::endl;
    ChessGame chess;
    chess.initPiece(ROOK_ENUM, WHITE, Position(0,0));
    chess.displayBoard();
    std::cout << "Expect no error messages" << std::endl;
    chess.makeMove(Position(0, 0), Position (0, 6));
    chess.makeMove(Position(0, 6), Position (6, 6));
    chess.makeMove(Position(6, 6), Position (6, 0));
    chess.makeMove(Position(6, 0), Position (0, 0));
    std::cout << "Expect invalid move" << std::endl;
    chess.makeMove(Position(0, 0), Position(1, 1));
    std::cout << std::endl << std::endl;
}

void bishopTest() {
    std::cout << "TESTING BISHOP" << std::endl;
    ChessGame chess;
    chess.initPiece(BISHOP_ENUM, WHITE, Position(3, 3));
    chess.displayBoard();
    std::cout << "Expect no error messages" << std::endl;
    chess.makeMove(Position(3, 3), Position(5, 5));
    chess.makeMove(Position(5, 5), Position(7, 3));
    chess.makeMove(Position(7, 3), Position(5, 1));
    chess.makeMove(Position(5, 1), Position(3, 3));
    std::cout << "Expect invalid move" << std::endl;
    chess.makeMove(Position(3, 3), Position(0, 3));
    std::cout << std::endl << std::endl;
}

void knightTest() {
    std::cout << "TESTING KNIGHT" << std::endl;
    ChessGame chess;
    chess.initPiece(KNIGHT_ENUM, WHITE, Position(2, 3));
    chess.displayBoard();
    std::cout << "Expect no error messages" << std::endl;
    chess.makeMove(Position(2, 3), Position(4, 4));
    chess.makeMove(Position(4, 4), Position(2, 3));
    chess.makeMove(Position(2, 3), Position(0, 4));
    chess.makeMove(Position(0, 4), Position(2, 3));
    chess.makeMove(Position(2, 3), Position(3, 5));
    chess.makeMove(Position(3, 5), Position(2, 3));
    chess.makeMove(Position(2, 3), Position(1, 5));
    chess.makeMove(Position(1, 5), Position(2, 3));
    std::cout << "Expect two invalid moves" << std::endl;
    chess.makeMove(Position(2, 3), Position(2, 5));
    chess.makeMove(Position(2, 3), Position(0, 3));
    std::cout << std::endl << std::endl;
}

void queenTest() {
    std::cout << "TESTING QUEEN" << std::endl;
    ChessGame chess;
    chess.initPiece(QUEEN_ENUM, WHITE, Position(3, 3));
    chess.displayBoard();
    std::cout << "Expect no error messages" << std::endl;
    chess.makeMove(Position(3, 3), Position(5, 5));
    chess.makeMove(Position(5, 5), Position(3, 3));
    chess.makeMove(Position(3, 3), Position(1, 5));
    chess.makeMove(Position(1, 5), Position(3, 3));
    chess.makeMove(Position(3, 3), Position(3, 6));
    chess.makeMove(Position(3, 6), Position(3, 3));
    chess.makeMove(Position(3, 3), Position(6, 3));
    chess.makeMove(Position(6, 3), Position(3, 3));
    std::cout << "Expect invalid move" << std::endl;
    chess.makeMove(Position(3, 3), Position(4, 5));
    std::cout << std::endl << std::endl;
}

void kingTest() {
    std::cout << "TESTING KING" << std::endl;
    ChessGame chess;
    chess.initPiece(KING_ENUM, WHITE, Position(3, 3));
    chess.displayBoard();
    std::cout << "Expect no error messages" << std::endl;
    chess.makeMove(Position(3, 3), Position(3, 4));
    chess.makeMove(Position(3, 4), Position(3, 3));
    chess.makeMove(Position(3, 3), Position(4, 4));
    chess.makeMove(Position(4, 4), Position(3, 3));
    chess.makeMove(Position(3, 3), Position(4, 3));
    chess.makeMove(Position(4, 3), Position(3, 3));
    chess.makeMove(Position(3, 3), Position(4, 2));
    chess.makeMove(Position(4, 2), Position(3, 3));
    std::cout << "Expect 2 invalid moves" << std::endl;
    chess.makeMove(Position(3, 3), Position(5, 3));
    chess.makeMove(Position(3, 3), Position(5, 5));
    std::cout << std::endl << std::endl;
}

void captureTest() {
    std::cout << "TESTING CAPTURE" << std::endl;
    ChessGame chess;
    chess.initPiece(PAWN_ENUM, WHITE, Position(3, 3));
    chess.initPiece(PAWN_ENUM, BLACK, Position(4, 4));
    std::cout << "Expect no error messages" << std::endl;
    chess.makeMove(Position(3, 3), Position(4, 4));
    std::cout << "Expect only white pawn at e5" << std::endl;
    chess.displayBoard();
    std::cout << "Expect invalid move" << std::endl;
    chess.initPiece(PAWN_ENUM, BLACK, Position(4, 5));
    chess.makeMove(Position(4, 4), Position(4, 5));
    std::cout << "Expect white pawn at e5 and black pawn at e6" << std::endl;
    chess.displayBoard();
    std::cout << std::endl << std::endl;
}

void checkTest() {
    std::cout << "TESTING CHECK" << std::endl;
    ChessGame chess;
    chess.initPiece(ROOK_ENUM, WHITE, Position(0, 0));
    chess.initPiece(KING_ENUM, BLACK, Position(1, 7));
    std::cout << "Expect invalid move due to check" << std::endl;
    chess.setTurn(1);
    chess.makeMove(Position(1, 7), Position(0, 7));
    std::cout << "Expect white rook at a1 and black king at b8" << std::endl;
    chess.displayBoard();
    std::cout << std::endl << std::endl;
}

void checkMateTest() {
    std::cout << "TESTING CHECKMATE" << std::endl;
    ChessGame chess;
    chess.initPiece(ROOK_ENUM, WHITE, Position(0, 0));
    chess.initPiece(QUEEN_ENUM, WHITE, Position(3, 3));
    chess.initPiece(KING_ENUM, BLACK, Position(0, 6));
    std::cout << "Expect checkmate" << std::endl;
    chess.makeMove(Position(3, 3), Position(1, 5));
    if (chess.checkMate()) {
        std::cout << "Checkmate!" << std::endl;
    }
    chess.displayBoard();
}

void staleMateTest() {
    std::cout << "TESTING STALEMATE" << std::endl;
    ChessGame chess;
    chess.initPiece(ROOK_ENUM, WHITE, Position(1, 0));
    chess.initPiece(ROOK_ENUM, WHITE, Position(7, 6));
    chess.initPiece(KING_ENUM, BLACK, Position(0, 6));
    std::cout << "Expect stalemate" << std::endl;
    chess.setTurn(1);
    chess.makeMove(Position(0, 6), Position(0, 7));
    chess.setTurn(1);
    std::cout << chess.checkMate() << std::endl;
    if (chess.checkMate() == 2) {
        std::cout << "Stalemate!" << std::endl;
    }
    chess.displayBoard();
}

//main function for running tests. To make things clearer, just comment out tests
//that you do not want to run.
int main() {
    pawnTest();
    rookTest();
    bishopTest();
    knightTest();
    queenTest();
    kingTest();
    captureTest();
    checkTest();
    checkMateTest();
    staleMateTest();
    return 0;
}
